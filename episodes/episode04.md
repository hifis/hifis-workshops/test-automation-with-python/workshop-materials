<!--
SPDX-FileCopyrightText: 2020 German Aerospace Center (DLR)

Text is licensed under:
SPDX-License-Identifier: CC-BY-4.0

Code snippets are licensed under:
SPDX-License-Identifier: MIT
-->

# HTML Parser: Test Isolation with Test Doubles

Complex systems usually have connections and inter-dependencies with other modules as it is shown in the next figure.
In context of test automation, this can lead to problems such as hard-to-write test setups, non-deterministic tests, and slow tests.

![Unit Isolation](images/unit-isolation.svg)

We have similar problems with the HTML parser tool.
The following figure shows the typical execution sequence when we want to parse a HTML document retrieved from a Web server.

![HTML Parser Sequence Diagram](images/html-parser-sequence-diagram.png)

`main.py` creates the `HtmlParser` and the `HttpFetcher` instances and orchestrates the whole process.
When the `extract_links` method is invoked, the `HtmlParser` instance uses the `HttpFetcher` instance to retrieve a HTML document from a given URL.
The retrieved HTML document gets parsed and the extracted links are printed on the command-line.

Testing this use case leads to following problems:
- The server access is much slower than a local access which reduces the speed of our test suite.
- We might not have any control over the server.
  I.e., we have only limited options to control returned data.
  Even worse, the server could be unavailable at any time.
  These aspects can lead to non-deterministic tests which fail although the code is still working.

A solution to these problems are [test doubles](https://www.martinfowler.com/bliki/TestDouble.html) which can be used to mimic the behavior of "real" objects.

## Hierarchy of Test Doubles

There are different test double types which address specific needs.
You should always try to use the test double with the least possible features to minimize complexity and later maintenance effort.

### Dummy Objects

*Dummy objects* are passed to functions and methods but are never really used.
Typically, you use them to fill the parameter list.
In Python, usually `None` is used in such a case.
Sometimes, you have to pass a more specific dummy object.
For example, if the parameter is checked for a certain type.

### Stub Objects

*Stub objects* provide canned answers.
They can be used like objects which they replace.
They do not implement any logic and only provide the results for the calls required by the corresponding test.

### Fake Objects

*Fake objects* behave quite similar like the objects which they replace.
They contain an implementation that is usually not suitable for production use.
For example, they might take shortcuts in the implementation, provide only static data, or only support a certain parameter space.
A typical example are in-memory databases. 


### Mock Objects

*Mock objects* are very similar to stub objects in the way that they also provide canned answers without implementing any logic.
Additionally, they can be configured to enforce certain conditions such as the invocation order of methods.
Finally, they can be used for introspection.
I.e., you can query a mock object whether specific methods were called in the expected order or with the required parameters.

### Usage Recommendations

You have to decide on the specific situation which test double is suited most (order implicates applicability):
 - **External system which is out of control:** fake object, record and play libraries (kind of automatically created/updatable stub), stub, mock
 - **Slow tests because of external system access:** fake object, stub / mock + minimal tests with real dependency
 - **It is hard to bring the system under test into a specific state:** stub, mock
 - **Missing components:** dummy object, stub, mock
 - **Non-observable state:** mock


## HTML Parser: Improving the Remote Access Test with a Fake Object

In the [last episode](episode03.md), we added a new characterization test which checks the parsing of a HTML document retrieved from a Web server.
This test is a bit problematic because it slows down our test suite and we are not in control of this server.
Thus, we improve the test case by using a **fake object**.
The pytest plugin [pytest-httpserver](https://pytest-httpserver.readthedocs.io/en/latest/) can start a real local HTTP server which can be programmed to respond to specific requests.

We start from the existing test case:

```python
def test_parse_remotefile_success(capfd):
    # Define URL
    url = "https://www.dlr.de"

    # Run the command
    exit_code = _run_main_script([url])

    # Make sure it is successfully run
    assert exit_code == 0
    assert capfd.readouterr()[0].startswith("DE/Home/home_node.html#sprungmarkeContent")
```

We adapt this test case to use a local HTTP server instead of a file as source.
This way we can test if the script works with remote data.

- Change the name of the test to `test_parse_remotefile_success`
- Add another fixture `httpserver` as a parameter to the test.
  `tmpdir` is no longer needed.
- Configure the `httpserver` fixture to deliver given data for `/test.html`:
  ```python
    content = "<html><body><a href='new_link.html'>Test</a></body></html>"
    httpserver.expect_request("/test.html").respond_with_data(content)
  ```
- Call the script with the URL of the `httpserver`.

The resulting tests looks as follows:

```python
def test_parse_remotefile_success(httpserver, capfd):
    # Create a valid HTML document and serve it via the fake HTTP server
    content = "<html><body><a href='new_link.html'>Test</a></body></html>"
    httpserver.expect_request("/test.html").respond_with_data(content)

    # Run the command
    exit_code = _run_main_script([httpserver.url_for("/test.html")])

    # Make sure it is successfully run
    assert exit_code == 0
    assert capfd.readouterr()[0].startswith("new_link.html")
```

The pytest plugin provides a test fixture named `httpserver`.
This object can be used to define an expected request for which we return the prepared HTML content.
Then, we can determine the actual URL and pass it to our HTML parser tool.

The advantages of this fake object are that we are in control of its behavior and that we still test everything using a real HTTP stack.

## HTML Parser: Testing the Parser Module

Next, we want to test the link extraction behavior of the `HtmlParser` class in isolation.
For this purpose, we use a stub object to replace the required `Fetcher` instance.
The stub object is configured using the library `unittest.mock`.

```python
from unittest import mock

import pytest

from html_parser.fetcher import Fetcher, FetcherError
from html_parser.parser import HtmlParser, HtmlParserError


class TestHtmlParser:
    def setup_method(self, _):
        self._fetcher_mock = mock.Mock(spec=Fetcher)
        self._parser = HtmlParser(self._fetcher_mock)
```
In `setup_method` we define a mock which replaces a real `Fetcher` instance.
This mock exposes all methods of `Fetcher` allowing us to configure the expected behaviour.
With the help of this mock, we construct the `HtmlParser` instance under test.
Now, we can use the mock to provide the required behavior in every test case.

The next step is to use the created object within the test cases.
The first test case shall test the successful extraction of a link.

```python
    def test_extract_links_success(self):
        # Configure the mock
        self._fetcher_mock.retrieve.return_value = "<a href='/index.html'>index.html</a>"

        # Call the method to test
        extracted_links = self._parser.extract_links()

        # Check the assertions
        assert len(extracted_links) == 1
        assert extracted_links[0] == "/index.html"
```

In the `test_extract_links_success` test case, we configure our `Fetcher` mock to return valid HTML when the `retrieve` method is called.
This can be done using the `return_value` attribute of the (mocked) method in question.

The second test case shall test the behavior of the `HtmlParser` when a `FetcherError` is raised within the Fetcher.
The expected behavior is that the `HtmlParser` will raise an `HtmlParserError`.

```python
    def test_extract_links_fetcher_error(self):
        # Configure the mock
        self._fetcher_mock.retrieve.side_effect = FetcherError

        # Call the method to test and check for the expected error
        with pytest.raises(HtmlParserError):
            self._parser.extract_links()
```

In the `test_extract_links_fetcher_error` test case, we used the `side_effect` attribute of the `retrieve` method of `Fetcher`.
This will trigger a `FetcherError` being raised when the method is called.

### Exercise

- Please add further test cases to `parser_test.py`, for example, to test:
  - Parsing an invalid HTML document. \
    Use `<html></html` as invalid HTML document.
  - Parsing a HTML document which contains no links. \
    Use `<html></html>` as empty HTML document.
- Please run all tests to make sure that everything works as expected.
- You can also use the example code on branch [02-add-further-mocked-parser-tests](https://gitlab.com/hifis/hifis-workshops/test-automation-with-python/html-parser/-/tree/02-add-further-mocked-parser-tests) as an starting point for this exercise.

## Key Points

- Test doubles can be pretty handy to address specific test challenges.
  But try to use the real dependencies as long as it is reasonable.
- Decide on the specific situation which kind of test double is suited most.
