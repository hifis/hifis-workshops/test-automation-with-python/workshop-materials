<!--
SPDX-FileCopyrightText: 2020 German Aerospace Center (DLR)

Text is licensed under:
SPDX-License-Identifier: CC-BY-4.0

Code snippets are licensed under:
SPDX-License-Identifier: MIT
-->

# HTML Parser: Analyzing the Code Coverage

Code coverage is a metric which indicates the degree to which the source code is checked by tests.
This makes it possible to assess the effectiveness of the test cases.
Particularly, you can use it to find "test holes" in your code.

## Code Coverage Types

For a better illustration, please consider the following code example and its control flow:

<table>
<tr><th>Code</th><th>Control Flow</th></tr>
<tr>
<td>


```python
def check(x):
    if x > 0:
        y = -x
    else:
        y = x

    while y < 0:
        y += 1
```
</td>
<td>

![Control Flow](images/control-flow-coverage.png)

</td>
</tr>
</table>

Typically, one can distinguish the following code coverage types:

- **Statement Coverage (C0):**
  - 100% coverage means every statement is executed once.
  - 2 test cases (e.g., `x = 1`, `x = -1`) are required to achieve 100% statement coverage in the example.
- **Branch Coverage (C1):**
  - 100% coverage means every branch is executed once.
  - 2 test cases (e.g., `x = 1`, `x = -1`) are required to achieve 100% branch coverage in the example.
- **Path Coverage (C2):**
  - 100% coverage means every possible code path is executed once.
  - An infinite number of test cases would be required to achieve 100% path coverage in the example.

In practice, one usually considers **statement** and **branch** coverage.

## Analyzing the Code Coverage of the HTML Parser Tool

[Coverage.py](https://coverage.readthedocs.io) is a mature and feature-rich tool for measuring code coverage of Python programs.
In the following, we use it in combination with `pytest` to analyze the code coverage of the HTML parser tool.
For that purpose, we use the example code on branch [03-check-code-coverage](https://gitlab.com/hifis/hifis-workshops/test-automation-with-python/html-parser/-/tree/03-check-code-coverage)
but you can still use the code from the preceding episode.

First, we want to determine the current **statement coverage**.
For that purpose, please run the following command:

```
python -m pytest --cov=html_parser tests
============================= test session starts =============================
platform win32 -- Python 3.6.10, pytest-6.1.1, py-1.9.0, pluggy-0.13.1
rootdir: C:\Users\<USERNAME>\html-parser
plugins: cov-2.10.1, httpserver-0.3.5
collected 7 items

tests\main_test.py ..                                                    [ 28%]
tests\parser_test.py .....                                               [100%]

---------- coverage: platform win32, python 3.6.10-final-0 -----------
Name                      Stmts   Miss  Cover
---------------------------------------------
html_parser\__init__.py       0      0   100%
html_parser\fetcher.py       26      5    81%
html_parser\main.py          17      4    76%
html_parser\parser.py        20      0   100%
---------------------------------------------
TOTAL                        63      9    86%


============================== 7 passed in 3.79s ===============================
```

It produces a coverage report on the console.
We can see that we have already achieved a pretty good statement coverage.
In addition, we see that some aspects in `fetcher.py` and `main.py` seem to be untested.

Now, we want to examine the **branch coverage** as follows:

```
python -m pytest --cov=html_parser --cov-branch tests
============================= test session starts =============================
platform win32 -- Python 3.6.10, pytest-6.1.1, py-1.9.0, pluggy-0.13.1
rootdir: C:\Users\<USERNAME>\html-parser
plugins: cov-2.10.1, httpserver-0.3.5
collected 7 items

tests\main_test.py ..                                                    [ 28%]
tests\parser_test.py .....                                               [100%]

---------- coverage: platform win32, python 3.6.10-final-0 -----------
Name                      Stmts   Miss Branch BrPart  Cover
-----------------------------------------------------------
html_parser\__init__.py       0      0      0      0   100%
html_parser\fetcher.py       26      5      2      0    82%
html_parser\main.py          17      4      6      1    78%
html_parser\parser.py        20      0      4      1    96%
-----------------------------------------------------------
TOTAL                        63      9     12      2    85%


============================== 7 passed in 3.02s ==============================
```

We can see that the coverage measures for `fetcher.py` and `main.py` slightly increased but for `parser.py` it dropped slightly.
The reason is that `coverage.py` includes the number of branches in the [overall coverage measure calculation](https://coverage.readthedocs.io/en/coverage-5.3/faq.html#q-how-is-the-total-percentage-calculated).
Particularly, the coverage drop of `parser.py` results from a missed branch.

Finally, we want to take a closer look at the results and create a HTML report for that purpose as follows:

```
python -m pytest --cov=html_parser --cov-branch --cov-report=html tests
============================= test session starts =============================
platform win32 -- Python 3.6.10, pytest-6.1.1, py-1.9.0, pluggy-0.13.1
rootdir: C:\Users\<USERNAME>\html-parser
plugins: cov-2.10.1, httpserver-0.3.5
collected 7 items

tests\main_test.py ..                                                    [ 28%]
tests\parser_test.py .....                                               [100%]

---------- coverage: platform win32, python 3.6.10-final-0 -----------
Coverage HTML written to dir htmlcov


============================== 7 passed in 3.79s ==============================
```

The report is written into the directory `./htmlcov`.
On this basis, we can take a closer look and find out which parts of the code could require further tests.
The following figure shows the detailed report for the `main.py` with the following indicators:

- **Green**: Indicates executed statements.
- **Red**: Indicates missed statements.
- **Yellow**: Indicates missed branches.

![Coverage Report for main.py](images/coverage-report-main.png)

### Excercise

- Please examine the coverage report for every Python module.
  What aspects of the code could require further tests?
- The report indicates a missing special case: `<a>` tags without `href` attributes.
  Please add a test case to `parser_test.py` which covers this special case and check its effect on the coverage report.

## Recommendations for Using Code Coverage

- You should use code coverage as a tool to detect room for testing improvements.
  Particularly, it helps you to identify untested code which is complex and may change in near future.
  It makes sense to make filling such "test holes" a priority.
- In practice, statement and branch coverage are typically used.
  Branch coverage gives you a bit more confidence as it checks the execution of the code branches.
  It is hard to give recommendations for absolute coverage numbers but a statement/branch coverage below 50% typically indicates a lack of testing.
- Do not concentrate on pure coverage numbers.
  It is important that you have proper fine-grained assertions in place which you typically establish via small and medium tests.
  For that purpose, it makes sense to check the combined coverage achieved with small and medium tests separately. 
- As with every software metric:
  - Please make sure to automatically collect and regularly check the metric trend.
    It helps you identify changes and potential problems early on.
  - Please avoid comparison of absolute numbers with other software projects as every software has its specific context.
  - Please avoid setting absolute metric goals as it sets wrong incentives and might lead to negative consequences for the overall code quality.

## Key Points

- Use code coverage as a tool to detect room for improvement ("test holes").
- `coverage.py` is an excellent tool for measuring and analyzing code coverage of Python programs.
