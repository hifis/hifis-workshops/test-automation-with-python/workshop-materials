<!--
SPDX-FileCopyrightText: 2020 German Aerospace Center (DLR)
SPDX-FileCopyrightText: 2020 Helmholtz-Zentrum Dresden-Rossendorf (HZDR)

Text is licensed under:
SPDX-License-Identifier: CC-BY-4.0

Code snippets are licensed under:
SPDX-License-Identifier: MIT
-->

# Introduction to pytest

Python already provides a built-in [unittest](https://docs.python.org/3/library/unittest.html) framework.
However, it has some shortcomings:

- Limited automatic test discovery.
- Bloated boiler-plate code.
- Clunky assertions.

Thus, we selected [pytest](https://docs.pytest.org/en/stable/) as our primary testing framework.
It is compatible to `unittest` and follows a typical [xUnit approach](https://en.wikipedia.org/wiki/XUnit) but tries to stay *pythonic*.
In addition, it removes *all* listed shortcomings and provides many useful plugins and extensions (e.g., parallel test execution, parametric tests, etc.).

## First Simple Test

Writing tests with `pytest` is similar to writing Python functions.
Test functions should be prefixed with `test_` or have a `_test` suffix:

<table>
<tr><th>pytest</th><th>unittest</th></tr>
<tr><td>

```python
from math import factorial

def test_factorial_success():
    assert factorial(1) == 1
```

</td><td>

```python
import unittest

from math import factorial

class TestFactorial(unittest.TestCase):
    def testSuccess(self):
        self.assertEqual(factorial(1), 1)
```

</td></tr></table>

## Test Driven Development

We will use the Test Driven Development (TDD) approach to write our first tests with `pytest`.
TDD is a repeated / circular micro-workflow in which a developer writes tests first before any implementations and changes in production code are made.

The workflow is as follows:

1. Write a test that fails ("**Red**").
2. Write just enough code that the test passes ("**Green**").
3. Refactor and improve the code without changing the behaviour of the code ("**Refactor**").

This diagram below is an illustration of the workflow.

<img src="images/tdd_cycle.png" alt="Test Driven Development Cycle" title="Test Driven Development Cycle" width="166" height="166" />

These are a couple of benefits you gain from practicing TDD:

1. You get immediate and fast feedback.
2. It simplifies error localisation.
3. It results in testable units of code that follow good coding practice.
4. It creates a safety-net for changes and refactorings to be made in production code.
5. It saves you time and trouble in the mid- to long-term.
6. It increases the value of your software product.
7. Testing and refactoring is already part of the development process.

**Note:** TDD is not a replacement for other development and testing approaches but a complement.

## Testing the Fibonacci Sequence

Fibonacci sequence is known to be the following infinite series of numbers:

> 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, ...

It can be expressed with the following recursive formula:

```math
F_0 = F_1 = 1 \\
F_n = F_{n - 1} + F_{n - 2} \\
n \in \N

```

In the following exercise we want to apply a *test driven development*[^1] approach to implement and test Fibonacci sequence in Python.

We need two files for this:

- `fibo.py` is the Python module to implement Fibonacci sequence.
- `fibo_test.py` contains the tests for our implementation.

[^1]: > A complete guide to *test driven development* is beyond the scope of this course.
      > It is sufficient to know that we follow two simple rules:
      >
      > 1. Write the tests before you implement the function.
      > 2. Only implement what you really have tested so far.

### Step 1: Simple Tests

- Create a file `fibo_test.py` and add the following code:

```python
from fibo import fibo

def test_fibo_start():
    assert fibo(0) == 1
```

- Execute the test with `pytest`:

```bash
$ pytest fibo_test.py
============================= test session starts ==============================
platform msys -- Python 3.7.4, pytest-5.1.2, py-1.8.1, pluggy-0.13.1
rootdir: /home/<user>/pytest
plugins: cov-2.7.1
collected 0 items / 1 errors

==================================== ERRORS ====================================
________________________ ERROR collecting fibo_test.py _________________________
ImportError while importing test module '/home/<user>/pytest/fibo_test.py'.
Hint: make sure your test modules/packages have valid Python names.
Traceback:
fibo_test.py:1: in <module>
    from fibo import fibo
E   ModuleNotFoundError: No module named 'fibo'
!!!!!!!!!!!!!!!!!!! Interrupted: 1 errors during collection !!!!!!!!!!!!!!!!!!!!
=============================== 1 error in 0.21s ===============================
```

We get an error as there is no implementation to test yet.
Hence, we start to implement our Fibonacci sequence calculation.

- Create a file `fibo.py` with the following content:

```python
def fibo(n):
    return 1
```

- Again, run the test:

```bash
$ pytest fibo_test.py
============================= test session starts ==============================
platform msys -- Python 3.7.4, pytest-5.1.2, py-1.8.1, pluggy-0.13.1
rootdir: /home/<user>/pytest
plugins: cov-2.7.1
collected 1 item

fibo_test.py .                                                           [100%]

============================== 1 passed in 0.02s ===============================
```

We see that we have a first working test.
Now, let us extend the test.

- Modify `test_fibo_start` to read like the following:

```python
def test_fibo_start():
    assert fibo(0) == 1
    assert fibo(1) == 1
```

Now we have ensured that our starting conditions are met ($`F_0 = F_1 = 1`$).
But there are more numbers in the sequence, so let us test (some of) them, too.

- Add the following test to `fibo_test.py`:

```python
def test_fibo_2():
    assert fibo(2) == 2
```

- Run the tests again

```bash
$ pytest fibo_test.py
============================= test session starts ==============================
platform msys -- Python 3.7.4, pytest-5.1.2, py-1.8.1, pluggy-0.13.1
rootdir: /home/<user>/pytest
plugins: cov-2.7.1
collected 2 items

fibo_test.py .F                                                          [100%]

=================================== FAILURES ===================================
_________________________________ test_fibo_2 __________________________________

    def test_fibo_2():
>       assert fibo(2) == 2
E       assert 1 == 2
E        +  where 1 = fibo(2)

fibo_test.py:10: AssertionError
========================= 1 failed, 1 passed in 0.16s ==========================
```

We get our first failed test.
We can see in the output that our last test failed.
In addition, we can see which assertion was not met and even where the wrong value originated.
So let us fix our implementation.

- Change `fibo.py` to read as follows:

```python
def fibo(n):
    if n < 2:
        return 1
    else:
        return 2
```

While this is clearly not the correct implementation, it is good enough to fulfill our tests.

- Run the tests again.

```bash
$ pytest fibo_test.py
============================= test session starts ==============================
platform msys -- Python 3.7.4, pytest-5.1.2, py-1.8.1, pluggy-0.13.1
rootdir: /home/<user>/pytest
plugins: cov-2.7.1
collected 2 items

fibo_test.py ..                                                          [100%]

============================== 2 passed in 0.02s ===============================
```

#### Exercise

- Add another test to `fibo_test.py` to calculate `fibo(3)`.

```python
def test_fibo_3():
    assert fibo(3) == 3
```

- Fix your implementation of `fibo(n)` for the last test to succeed.
  Choose the minimalistic solution, we will add a proper implementation in a minute.

```python
def fibo(n):
    if n < 2:
        return 1
    else:
        return n
```

### Step 2: Parametric Tests

Now, we are at a point where we could go on adding simple test cases for every sequence member: `test_fibo_4`, `test_fibo_5`, ...
However, this is not very convenient as there are lots of (i.e., infinite) possible tests.
On top, they are all of the same form:

```python
def test_fibo_n():
    assert fibo(n) == F_n
```

Luckily, *pytest* provides a feature to implement these kind of generalized tests.
It is called "paramterized test cases".

- At the beginning of `fibo_test.py` add the following line:

```python
import pytest
```

- Then, add the following test case to `fibo_test.py`:

```python
@pytest.mark.parametrize("n, F_n", [
    (4, 5), (5, 8), (6, 13)
])
def test_fibo_n(n, F_n):
    assert fibo(n) == F_n
```

- Execute the tests:

```bash
$ pytest fibo_test.py
============================= test session starts ==============================
platform msys -- Python 3.7.4, pytest-5.1.2, py-1.8.1, pluggy-0.13.1
rootdir: /home/<user>/pytest
plugins: cov-2.7.1
collected 6 items

fibo_test.py ...FFF                                                      [100%]

=================================== FAILURES ===================================
_______________________________ test_fibo_n[4-5] _______________________________

n = 4, F_n = 5

    @pytest.mark.parametrize("n, F_n", [
        (4, 5), (5, 8), (6, 13)
    ])
    def test_fibo_n(n, F_n):
>       assert fibo(n) == F_n
E       assert 4 == 5
E        +  where 4 = fibo(4)

fibo_test.py:23: AssertionError
_______________________________ test_fibo_n[5-8] _______________________________

n = 5, F_n = 8

    @pytest.mark.parametrize("n, F_n", [
        (4, 5), (5, 8), (6, 13)
    ])
    def test_fibo_n(n, F_n):
>       assert fibo(n) == F_n
E       assert 5 == 8
E        +  where 5 = fibo(5)

fibo_test.py:23: AssertionError
______________________________ test_fibo_n[6-11] _______________________________

n = 6, F_n = 11

    @pytest.mark.parametrize("n, F_n", [
        (4, 5), (5, 8), (6, 13)
    ])
    def test_fibo_n(n, F_n):
>       assert fibo(n) == F_n
E       assert 6 == 13
E        +  where 6 = fibo(6)

fibo_test.py:23: AssertionError
========================= 3 failed, 3 passed in 0.16s ==========================
```

Even though, we only added one test, *pytest* indicates three additional tests which all fail.
This is due to the `@pytest.mark.parametrize` annotation which tells *pytest* that this test depends on two parameters named `n` and `F_n`.
In the following, we define valid parameter combinations.
All of them will be tested as separate test cases.

Now, we need to fix our implementation again.

- Change `fibo.py` to read as follows:

```python
def fibo(n):
    if n < 2:
        return 1
    else:
        return fibo(n - 1) + fibo(n - 2)
```

- Run the tests again:

```bash
$ pytest fibo_test.py
============================= test session starts ==============================
platform msys -- Python 3.7.4, pytest-5.1.2, py-1.8.1, pluggy-0.13.1
rootdir: /home/<user>/pytest
plugins: cov-2.7.1
collected 6 items

fibo_test.py ......                                                      [100%]

============================== 6 passed in 0.04s ===============================
```

Now, we can easily add further sequence items to the test by simply extending the list of tuples for the parametrized test.

### Step 3: Choosing Test Cases

You could go forward by adding lots of additional test cases to the parametrized test.
But which test cases are still meaningful?

Choosing good test cases is in fact some kind of art.
A special caveat for the developer is to think of test cases that are beyond the implemented behavior.
One strength of the *test driven development* approach is that the developer always strives to add tests that explicitly break the current implemented behavior.
I.e., the process let us already think about more aspects than the simple "happy" case.

In order to choose "sensible" input data for your test cases you can think in
terms of *equivalence classes* and do a *boundary analysis*.
In the former case the developer partitions the whole range of 
possible values of input variables into disjoint bounds/classes for which the
Unit-Under-Test (UUT) behaves differently, that is, produces distinguishable
output.
This is usually a good choice because you do not want to create
numerous redundant test cases that test the same things, but choose 
representatives for each equivalence class.

The latter approach builds on these *equivalence classes* and offers a
heuristic how to choose these representative values for each class found.

As a general rule of thumb, you can choose the following input values to your
units and implement test cases for:

- values that lay **within** the specified bounds/class,
- values that lay **on** the specified bound/class,
- values that lay **outside** of the specified bounds/class.

If you have several parameters for your function under test, keep all values **within** the specified range and only vary one of them for every test case.
That way, you can better investigate the problems that might occur.

So let us go back to our example and try to break our `fibo.py` implementation.
Looking at the domain of Fibonacci sequence, we can easily identify the bounds:

![Numeric Domain](images/numeric-domain.svg)

So obviously, $`n < 0`$ is out of bounds.
To make our implementation *pythonic*, we expect a `ValueError` whenever we pass a number that is out of bounds.

- Add the following test to `fibo_test.py`:

```python
def test_fibo_negative():
    with pytest.raises(ValueError):
        fibo(-1)
```

- Execute the test and look at the results:

```bash
$ pytest fibo_test.py
============================= test session starts ==============================
platform msys -- Python 3.7.4, pytest-5.1.2, py-1.8.1, pluggy-0.13.1
rootdir: /home/<user>/pytest
plugins: cov-2.7.1
collected 7 items

fibo_test.py ......F                                                     [100%]

=================================== FAILURES ===================================
______________________________ test_fibo_negative ______________________________

    def test_fibo_negative():
        with pytest.raises(ValueError):
>           fibo(-1)
E           Failed: DID NOT RAISE <class 'ValueError'>

fibo_test.py:28: Failed
========================= 1 failed, 6 passed in 0.15s ==========================
```

You can see that *pytest* expected an exception to be raised.
However, our current implementation simply returned a number as if it would know what to do.

- Update `fibo.py` to check for negative values:

```python
def fibo(n):
    if n < 0:
        raise ValueError("Fibonacci sequence is only defined for non-negative n.")
    elif n < 2:
        return 1
    else:
        return fibo(n - 1) + fibo(n - 2)
```

- Run the tests again:

```bash
$ pytest fibo_test.py
============================= test session starts ==============================
platform msys -- Python 3.7.4, pytest-5.1.2, py-1.8.1, pluggy-0.13.1
rootdir: /home/<user>/pytest
plugins: cov-2.7.1
collected 7 items

fibo_test.py .......                                                     [100%]

============================== 7 passed in 0.03s ===============================
```

#### Exercise

Fibonacci sequence is only defined for non-negative integer values.
Until now, our implementation ensures we do not pass any negative number.
However, it still would accept rational numbers as input.

- Write a test that checks if `fibo()` raises an `TypeError` if a non-integer value is passed.

```python
def test_fibo_float():
    with pytest.raises(TypeError):
        fibo(3.14)
```

- Update the implementation to successfully pass the test.

```python
def fibo(n):
    if not isinstance(n, int):
        raise TypeError("Fibonacci sequence is only defined for integer values.")
    elif n < 0:
        raise ValueError("Fibonacci sequence is only defined for positive n.")
    elif n < 2:
        return 1
    else:
        return fibo(n - 1) + fibo(n - 2)
```

### Step 4: Refactoring Supported by Tests

Now, we have a pretty good implementation for Fibonacci sequence and lots of tests in place.
Let us do a stress test:

- Add the following test to `fibo_test.py`:

```python
def test_fibo_huge():
    assert fibo(35) == 14930352
```

- Excute the tests again:

```bash
$ pytest fibo_test.py
============================= test session starts ==============================
platform msys -- Python 3.7.4, pytest-5.1.2, py-1.8.1, pluggy-0.13.1
rootdir: /home/<user>/pytest
plugins: cov-2.7.1
collected 9 items

fibo_test.py .........                                                   [100%]

============================== 9 passed in 5.38s ===============================
```

You should note an increase in the runtime.
Even if it is still pretty fast, it is noticeable slower than before.
This is due to our recursive implementation.
The bigger `n`, the longer it will take to calculate.
But as we are pretty convenient that our implementation will still hold due to all the other tests, we can mark this specific test as being slow.

- Create a file `pytest.ini` with the following content:

```ini
[pytest]
markers =
    slow: Tests that take longer to execute.
```

- Change `fibo_test.py` by adding an annotation to `fibo_test_huge()`:

```python
@pytest.mark.slow
def test_fibo_huge():
    assert fibo(35) == 14930352
```

- Now we can skip `slow` tests by running:

```bash
$ pytest -m "not slow" fibo_test.py
============================= test session starts ==============================
platform msys -- Python 3.7.4, pytest-5.1.2, py-1.8.1, pluggy-0.13.1
rootdir: /home/<user>/pytest
plugins: cov-2.7.1
collected 9 items / 1 deselected / 8 selected

fibo_test.py ........                                                    [100%]
================= 8 passed, 1 deselected, 1 warnings in 0.04s ==================
```

#### Exercise

- Refactor `fibo.py` to avoid recursive calls.
  Use a list and a loop to implement the calculation.

```python
_fibo = [1, 1]


def fibo(n):
    if not isinstance(n, int):
        raise TypeError("Fibonacci sequence is only defined for integer values.")
    elif n < 0:
        raise ValueError("Fibonacci sequence is only defined for positive n.")
    else:
        while len(_fibo) <= n:
            _fibo.append(_fibo[-1] + _fibo[-2])
        return _fibo[n]
```

- Test your new implementation against your test suite.
  See how the runtime of your tests changes.

## Tips

- Make your test cases small and focussed.
  You should not combine too many assertions within one test case.
  Especially assertions should not be dependent on each other or their order.
- When writing test cases, pay special attention to boundary values.
- For test targets that accept multiple input values, only change one of them for each test.
- Make your assertions static, i.e., do not calculate the expected result within your test case.
- Avoid nested code in your test cases.
  If you have more than tree levels of indentation, there might be something wrong.
- Test cases should not need any branches.
  If you have a loop or `if` in your test case, try to split it up or parametrize it.
  
## Key Points

- *pytest* is a good alternative to Python's `unittest` module.
  It reduces boilerplate code, integrates test discovery, and allows to write better readable assertions.
- Test cases are simple Python functions that start with `test_` or end with `_test`.
- You can use the `assert` statement to make assertions.
  *pytest* will provide useful error messages for them if they fail.
- Test cases can be parametrized using the decorator `@pytest.mark.parametrize(...)`.
- To test raised errors, you can use `with pytest.raises(...)` as context.
- You can add custom markers to filter test execution, for example, to skip slow tests.
