## License

Copyright © 2020 German Aerospace Center (DLR)

This work is licensed under multiple licenses:
- Texts and images are usually licensed under [CC-BY-4.0](LICENSES/CC-BY-4.0.txt).
- Source code is licensed under [MIT](LICENSES/MIT.txt).

Please see the individual files for more accurate information.

> **Hint:** We provided the copyright and license information in accordance to the [REUSE Specification 3.0](https://reuse.software/spec/).
